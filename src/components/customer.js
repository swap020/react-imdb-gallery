import React, {useState} from 'react';
import { useSelector, useDispatch } from "react-redux";
import user from '../assets/img/user.png';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
 

const Customer = (props) => {
  const ui = useSelector(state => state);
  const dispatch = useDispatch();
  const [profile, setProfile] = useState({...ui});
  const onChange = e => setProfile({...profile,  [e.target.name]: e.target.value})
  const {name, email, mobile, address} = profile;

  const setLocalData = (e) => {
    e.preventDefault();
    dispatch({ payload: profile, type: 'UPDATE' });
  }



  return (
    <div>
      <h1>Customer</h1>
      <div className="user">
         <Form onSubmit={setLocalData} as={Row}>
         <Form.Group as={Col} md="6">
            <Form.Label>Name</Form.Label>
            <Form.Control type="text" placeholder="John Doe" name="name" value={name} onChange={onChange} />
          </Form.Group>
          <Form.Group as={Col} md="6">
            <Form.Label>Email</Form.Label>
            <Form.Control type="email" placeholder="name@example.com" name="email" value={email} onChange={onChange} />
          </Form.Group>
          <Form.Group as={Col} md="6">
            <Form.Label>Mobile</Form.Label>
            <Form.Control type="number" placeholder="8888888888" name="mobile" value={mobile} onChange={onChange} />
          </Form.Group>
          <Form.Group as={Col} md="6">
            <Form.Label>Address</Form.Label>
            <Form.Control type="text" placeholder="10, Backer's Street" name="address" value={address} onChange={onChange} />
          </Form.Group>
          <Button variant="primary" className={user} type="submit" as={Col} md={{span: 4, offset: 4}}>
            Submit
          </Button>
        </Form>
      </div>
    </div>
  );
}

export default Customer;
