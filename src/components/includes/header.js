import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

const Header = () => {
  return (
 <Navbar bg="light" expand="lg">
  <Navbar.Brand href="/">Sample Task</Navbar.Brand>
  <Navbar.Toggle aria-controls="basic-navbar-nav" />
  <Navbar.Collapse id="basic-navbar-nav">
    <Nav className="mr-auto">
      <Nav.Link href="/">Profile</Nav.Link>
      <Nav.Link href="/gallery">Gallery</Nav.Link>
      <Nav.Link href="/customer">Customer</Nav.Link>
    </Nav>
  </Navbar.Collapse>
</Navbar>
  );
}

export default Header;