import React, {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Figure from 'react-bootstrap/Figure';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Navbar from 'react-bootstrap/Navbar';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';

const Gallery = () => {
  const [imgInfo, setImgInfo] = useState([])
  const [search, setSearch] = useState('inception')

  const searchMovie = (e) => {setSearch(e.target.value)};

  const header = {
  "x-rapidapi-host": "imdb-internet-movie-database-unofficial.p.rapidapi.com",
  "x-rapidapi-key": "", //Add Your Key
  "useQueryString": true
  }

  useEffect(() => {
    axios.get(`https://imdb-internet-movie-database-unofficial.p.rapidapi.com/search/${search}`, {headers: header}).then(res =>{
    setImgInfo(res.data.titles);
    }, err => console.log(err))
  }, [search]);

  return (
    <Container>
     <Navbar bg="light" expand="sm">
        <Form>
          <FormControl type="text" placeholder="Search Movies..." className="mr-sm-3" onChange={(e) => setSearch(e.target.value)}/>
        </Form>
    </Navbar>
      <Row>{
      imgInfo.length == 0 ? 'Loading...' :
      imgInfo.map(img => 
        <Col md={4} key={img.id}>
        <Link to={`/detail/${img.id}`}>
          <Figure>
            <Figure.Image
              width={171}
              height={180}
              alt="171x180"
              src={img.image}
            />
            <Figure.Caption>
              {img.title}
            </Figure.Caption>
          </Figure>
        </Link>
        </Col>
        )
      }</Row>
    </Container>
  );
}

export default Gallery; 